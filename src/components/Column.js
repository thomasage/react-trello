import React from 'react'
import PropTypes from 'prop-types'

import styles from './Column.module.scss'

export default function Column ({ children, name, onAdd, onDelete }) {
  function handleAdd () {
    const name = window.prompt('Carte :')
    if (!name || name === '') {
      return
    }
    onAdd(name)
  }

  return (
    <div className={styles.column}>
      <div className={styles.header}>
        <div className={styles.title}>{name}</div>
        <div className={styles.actions}>
          <button type="button" onClick={handleAdd}>
            <i className="fas fa-plus"/>
          </button>
          <button type="button" onClick={onDelete} className={styles.delete}>
            <i className="fas fa-trash"/>
          </button>
        </div>
      </div>
      <div className={styles.cards}>
        {children}
      </div>
    </div>
  )
}

Column.propTypes = {
  children: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
  onAdd: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}
