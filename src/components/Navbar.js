import React from 'react'
import PropTypes from 'prop-types'

import styles from './Navbar.module.scss'

export default function Navbar ({ onColumnAdd }) {
  function handleAdd () {
    const name = window.prompt('Colonne :')
    if (!name || name === '') {
      return
    }
    onColumnAdd(name)
  }

  return (
    <div className={styles.navbar}>
      Navbar
      <nav className={styles.actions}>
        <button type="button" onClick={handleAdd}>
          <i className="fas fa-plus"/>
        </button>
      </nav>
    </div>
  )
}

Navbar.propTypes = {
  onColumnAdd: PropTypes.func.isRequired
}
