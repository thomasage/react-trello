import React from 'react'
import PropTypes from 'prop-types'

import styles from './Board.module.scss'

export default function Board ({ children }) {
  return (
    <div className={styles.columns}>
      {children}
    </div>
  )
}

Board.propTypes = {
  children: PropTypes.array.isRequired
}
