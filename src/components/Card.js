import React from 'react'
import PropTypes from 'prop-types'

import styles from './Card.module.scss'

export default function Card ({ name, onDelete }) {
  return (
    <div className={styles.card}>
      <div>{name}</div>
      <div className={styles.actions}>
        <button type="button" onClick={onDelete}>
          <i className="fas fa-trash"/>
        </button>
      </div>
    </div>
  )
}

Card.propTypes = {
  name: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired
}
