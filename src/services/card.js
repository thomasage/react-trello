import { v4 as uuidv4 } from 'uuid'

/**
 * @param {string} column
 * @param {string} name
 * @returns {Promise<string>}
 */
export function cardAdd (column, name) {
  return new Promise(resolve => {
    const cards = JSON.parse(localStorage.getItem('cards')) ?? []
    const uuid = uuidv4()
    cards.push({
      column,
      name,
      uuid
    })
    localStorage.setItem('cards', JSON.stringify(cards))
    resolve(uuid)
  })
}

/**
 * @param {string} uuid
 * @returns {Promise}
 */
export function cardDelete (uuid) {
  return new Promise(resolve => {
    const cards = JSON.parse(localStorage.getItem('cards')) ?? []
    localStorage.setItem('cards', JSON.stringify(cards.filter(card => card.uuid !== uuid)))
    resolve()
  })
}

/**
 * @returns {Promise<Array>}
 */
export function cardGetAll () {
  return new Promise(resolve => {
    const cards = JSON.parse(localStorage.getItem('cards')) ?? []
    resolve(cards)
  })
}
