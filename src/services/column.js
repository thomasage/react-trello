import { v4 as uuidv4 } from 'uuid'
import { cardDelete, cardGetAll } from './card'

/**
 * @param {string} name
 * @returns {Promise<string>}
 */
export function columnAdd (name) {
  return new Promise(resolve => {
    const columns = JSON.parse(localStorage.getItem('columns')) ?? []
    const uuid = uuidv4()
    columns.push({
      name,
      uuid
    })
    localStorage.setItem('columns', JSON.stringify(columns))
    resolve(uuid)
  })
}

/**
 * @param {string} uuid
 * @returns {Promise}
 */
export function columnDelete (uuid) {
  return new Promise(resolve => {
    cardGetAll()
      .then(cards => cards.filter(card => card.column === uuid))
      .then(cards => cards.map(card => cardDelete(card.uuid)))
    const columns = JSON.parse(localStorage.getItem('columns')) ?? []
    localStorage.setItem('columns', JSON.stringify(columns.filter(column => column.uuid !== uuid)))
    resolve()
  })
}

/**
 * @returns {Promise<Array>}
 */
export function columnGetAll () {
  return new Promise(resolve => {
    const columns = JSON.parse(localStorage.getItem('columns')) ?? []
    resolve(columns)
  })
}
