import React, { useEffect, useState } from 'react'

import { cardAdd, cardDelete, cardGetAll } from './services/card'
import { columnAdd, columnDelete, columnGetAll } from './services/column'

import Board from './components/Board'
import Column from './components/Column'
import Card from './components/Card'
import Loading from './components/Loading'
import Navbar from './components/Navbar'

export default function App () {
  const [cards, setCards] = useState()
  const [columns, setColumns] = useState()

  useEffect(() => {
    refreshCards()
    refreshColumns()
  }, [])

  if (!columns || !cards) {
    return (
      <Loading/>
    )
  }

  function handleCardAdd (column, name) {
    cardAdd(column, name)
      .then(refreshCards)
  }

  function handleCardDelete (uuid) {
    cardDelete(uuid)
      .then(refreshCards)
  }

  function handleColumnAdd (name) {
    columnAdd(name)
      .then(refreshColumns)
  }

  function handleColumnDelete (uuid) {
    columnDelete(uuid)
      .then(refreshColumns)
  }

  function refreshCards () {
    cardGetAll()
      .then(setCards)
  }

  function refreshColumns () {
    columnGetAll()
      .then(setColumns)
  }

  function renderCard (card) {
    return (
      <Card
        key={card.uuid}
        onDelete={() => handleCardDelete(card.uuid)}
        name={card.name}
      />
    )
  }

  function renderCards (column) {
    return cards
      .filter(card => card.column === column)
      .map(renderCard)
  }

  function renderColumn (column) {
    return (
      <Column key={column.uuid}
              name={column.name}
              onAdd={name => handleCardAdd(column.uuid, name)}
              onDelete={() => handleColumnDelete(column.uuid)}>
        {renderCards(column.uuid)}
      </Column>
    )
  }

  function renderColumns () {
    return columns
      .map(renderColumn)
  }

  return (
    <>
      <Navbar
        onColumnAdd={handleColumnAdd}
      />
      <Board cards={cards}
             columns={columns}
             onDeleteCard={handleCardDelete}>
        {renderColumns()}
      </Board>
    </>
  )
}
